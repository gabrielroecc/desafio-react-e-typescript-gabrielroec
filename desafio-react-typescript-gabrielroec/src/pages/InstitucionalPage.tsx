import React from "react";
import { Header } from "../components/Header";
import { BreadCrumb } from "../components/BreadCrumb";
import style from "./Institucional.module.css";
import { PageContent } from "../components/PageContent";
import { Newsletter } from "../components/Newsletter";
import { Footer } from "../components/Footer";
import { FinalSection } from "../components/FinalSection";
const InstitucionalPage = () => {
  return (
    <div className={style["container-page"]}>
      <Header />
      <BreadCrumb />
      <PageContent />
      <Newsletter />
      <Footer />
      <FinalSection />
    </div>
  );
};
export { InstitucionalPage };
