import React from "react";
import ReactDOM from "react-dom/client";
import "./global.css";
import { InstitucionalPage } from "./pages/InstitucionalPage";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <InstitucionalPage />
  </React.StrictMode>
);
