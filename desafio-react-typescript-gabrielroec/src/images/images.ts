import logo from "./logo-m3-academy.png";
import searchicon from "./searchicon.png";
import menumobileicon from "./menu-mobile-icon.png";
import carticon from "./cart-icon.png";
import closemenumobile from "./close.png";
import breadcrumbhome from "./bread-crumb-home.png";
import arrowbreadcrumb from "./arrow-breadcrumb.png";
import socialsfb from "./socials-fb.png";
import socialsig from "./socials-ig.png";
import socialstt from "./socials-tt.png";
import socialsyt from "./socials-yt.png";
import socialsli from "./socials-li.png";
import visa from "./Visa.png";
import boleto from "./Boleto.png";
import diners from "./Diners.png";
import elo from "./Elo.png";
import hiper from "./Hiper.png";
import master from "./Master.png";
import pagseguro from "./Pagseguro.png";
import vtexpci from "./vtex-pci.png";
import vtexlogo from "./vtexlogo.png";
import m3logo from "./m3logo.png";

export default {
  logo,
  searchicon,
  menumobileicon,
  carticon,
  closemenumobile,
  breadcrumbhome,
  arrowbreadcrumb,
  socialsfb,
  socialsig,
  socialstt,
  socialsyt,
  socialsli,
  visa,
  boleto,
  diners,
  elo,
  hiper,
  master,
  pagseguro,
  vtexpci,
  vtexlogo,
  m3logo,
};
