import React from "react";
import style from "./FinalSection.module.css";
import images from "../images/images";
const FinalSection = () => {
  return (
    <div className={style["final-section-wrapper"]}>
      <div className={style["final-section-infotext"]}>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor
        </p>
      </div>
      <div className={style["final-section-payments-brand"]}>
        <img src={images.master} alt="" />
        <img src={images.visa} alt="" />
        <img src={images.diners} alt="" />
        <img src={images.elo} alt="" />
        <img src={images.hiper} alt="" />
        <img src={images.pagseguro} alt="" />
        <span className={style["boleto-logo"]}>
          <img src={images.boleto} alt="" />
        </span>
        <img className={style["vtexpci-logo"]} src={images.vtexpci} alt="" />
      </div>
      <div className={style["final-section-principal-brands"]}>
        <span>Powered by</span>
        <img className={style["vtex-logo"]} src={images.vtexlogo} alt="" />
        <span>Developed by</span>
        <img className={style["m3-logo"]} src={images.m3logo} alt="" />
      </div>
    </div>
  );
};

export { FinalSection };
