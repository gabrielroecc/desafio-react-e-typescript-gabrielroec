import React from "react";
import images from "../images/images";
import style from "./BreadCrumb.module.css";

const BreadCrumb = () => {
  return (
    <div className={style["bread-crumb-wrapper"]}>
      <div className={style["home-icon-wrapper"]}>
        <img src={images.breadcrumbhome} alt="" />
      </div>
      <div className={style["arrow-icon-wrapper"]}>
        <img src={images.arrowbreadcrumb} alt="" />
      </div>
      <div className={style["namepage-breadcrumb-wrapper"]}>
        <span>INSTITUCIONAL</span>
      </div>
    </div>
  );
};

export { BreadCrumb };
