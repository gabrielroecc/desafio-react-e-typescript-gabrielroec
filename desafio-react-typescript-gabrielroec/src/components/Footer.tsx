import React, { Component } from "react";
import style from "./Footer.module.css"
import { FooterElements } from "./FooterElements";
import { FooterLink } from "./FooterLink";
import { Socials } from "./Socials";


const Footer = () =>{



    return <div className={style["footer-wrapper"]}>
        <FooterElements title="Institucional">
            <FooterLink link="#">Quem Somos</FooterLink>
            <FooterLink link="#">Política de Privacidade</FooterLink>
            <FooterLink link="#">Segurança</FooterLink>
            <FooterLink link="#">Seja um Revendedor</FooterLink>
        </FooterElements>

        <FooterElements title="Dúvidas">
            <FooterLink link="#">Entrega</FooterLink>
            <FooterLink link="#">Pagamento</FooterLink>
            <FooterLink link="#">Trocas e Devolução</FooterLink>
            <FooterLink link="#">Dúvidas Frequentes</FooterLink>
        </FooterElements>

        <FooterElements title="Fale Conosco">
            <FooterLink link="#">Atendimento ao consumidor</FooterLink>
            <FooterLink link="#">(11) 4149 95</FooterLink>
            <FooterLink link="#">Atendiamento ao cliente</FooterLink>
            <FooterLink link="#">(11) 99433-8825</FooterLink>
        </FooterElements>

        <Socials/>
        
    </div>
}


export {Footer}