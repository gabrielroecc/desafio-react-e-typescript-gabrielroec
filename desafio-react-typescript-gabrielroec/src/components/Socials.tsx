import React from "react";
import style from "./Socials.module.css"
import images from "../images/images";

const Socials = () =>{
    return <div className={style["socials-wrapper"]}>
        <div className={style["socials-image-wrapper"]}>
            <img src={images.socialsfb} alt="" />
            <img src={images.socialsig} alt="" />
            <img src={images.socialstt} alt="" />
            <img src={images.socialsyt} alt="" />
            <img src={images.socialsli} alt="" />
        </div>
        <div className={style["footer-link-site"]}>
            <a href="#">www.loremipsum.com</a>
        </div>
    </div>
}

export {Socials}