import React, { useState } from "react";
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FormikHelpers,
  useField,
} from "formik";
import style from "./FormContact.module.css";
import FormSchema from "./schema/FormSchema";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  datadenascimento: string;
  tel: string;
  insta: string;
  termos: boolean;
}

const initialValues = {
  name: "",
  email: "",
  cpf: "",
  datadenascimento: "",
  tel: "",
  insta: "",
  termos: false,
};

const FormContact = () => {
  const handleFormikSubmit = (values: IFormikValues, actions: any) => {
    console.log(values);
    actions.resetForm();
  };

  return (
    <Formik
      validationSchema={FormSchema}
      onSubmit={handleFormikSubmit}
      initialValues={initialValues}
    >
      {({ errors, touched }) => (
        <div className={style["form-wrapper"]}>
          <Form className={style["form-contact"]}>
            <h1>PREENCHA O FORMULÁRIO</h1>
            <div>
              <label className={style["label-form"]} htmlFor="nome">
                Nome
              </label>
              <Field
                className={`${style["form-input"]} ${
                  errors.name && touched.name && style["invalid"]
                }`}
                id="name"
                name="name"
                placeholder="Seu nome completo"
              />
              <div className={style["feedback-wrapper"]}>
                <ErrorMessage
                  name="name"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>
            <div>
              <label className={style["label-form"]} htmlFor="email">
                E-mail
              </label>

              <Field
                className={`${style["form-input"]} ${
                  errors.email && touched.email && style["invalid"]
                }`}
                id="email"
                name="email"
                placeholder="Seu e-mail"
              />
              <div className={style["feedback-wrapper"]}>
                <ErrorMessage
                  name="email"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>
            <div>
              <label className={style["label-form"]} htmlFor="cpf">
                CPF
              </label>
              <Field
                className={`${style["form-input"]} ${
                  errors.cpf && touched.cpf && style["invalid"]
                }`}
                id="cpf"
                name="cpf"
                placeholder="000 000 000 00"
                maxLength="14"
                type="text"
              />
              <div className={style["feedback-wrapper"]}>
                <ErrorMessage
                  name="cpf"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>

            <div>
              <label className={style["label-form"]} htmlFor="datadenascimento">
                Data de nascimento
              </label>
              <Field
                className={`${style["form-input"]} ${
                  errors.datadenascimento &&
                  touched.datadenascimento &&
                  style["invalid"]
                }`}
                name="datadenascimento"
                id="datadenascimento"
                placeholder="00 . 00 . 0000"
              />
              <div className={style["feedback-wrapper"]}>
                <ErrorMessage
                  name="datadenascimento"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>

            <div>
              <label className={style["label-form"]} htmlFor="telefone">
                Telefone
              </label>
              <Field
                className={`${style["form-input"]} ${
                  errors.tel && touched.tel && style["invalid"]
                }`}
                id="cpf"
                name="tel"
                placeholder="(+00) 0000 0000"
              />
              <div className={style["feedback-wrapper"]}>
                <ErrorMessage
                  name="tel"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>

            <div>
              <label className={style["label-form"]} htmlFor="insta">
                Instagram
              </label>
              <Field
                className={`${style["form-input"]} ${
                  errors.tel && touched.tel && style["invalid"]
                }`}
                id="insta"
                name="insta"
                placeholder="@seuuser"
              />
              <div className={style["feedback-wrapper"]}>
                <ErrorMessage
                  name="insta"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>

            <div className={style["div-checkbox"]}>
              <label htmlFor="termos">Declaro que li e aceito</label>
              <Field id="termos" name="termos" type="checkbox" />
              <div
                className={`${style["feedback-wrapper"]} ${style["feedback-termos"]}`}
              >
                <ErrorMessage
                  name="termos"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </div>

            <button type="submit" className={style["form-button"]}>
              CADASTRAR
            </button>
          </Form>
        </div>
      )}
    </Formik>
  );
};

export { FormContact };
