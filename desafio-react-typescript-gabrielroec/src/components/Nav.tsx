import React from "react";
import style from "./Nav.module.css";
import { LiItem } from "./LiItem";

const Nav = () => {
  return (
    <div className={style["nav-wrapper"]}>
      <ul className={style["nav-list"]}>
        <LiItem link="#" text="Curso" />
        <LiItem link="#" text="Saiba mais" />
      </ul>
    </div>
  );
};

export { Nav };
