import React, {Children, ReactNode} from "react";
import style from "./FooterLink.module.css"

interface IProps{
    link:string
    children: ReactNode
}
const FooterLink = (props : IProps) =>{
    return <div>
    <li className={style["footer-link"]}>
        <a className={style["link"]} href={props.link}>{props.children}</a>
    </li>
</div>
}

export {FooterLink}

