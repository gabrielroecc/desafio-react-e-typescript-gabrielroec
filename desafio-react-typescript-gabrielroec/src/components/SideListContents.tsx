import React from "react";
import style from "./SideListContents.module.css";

interface props {
  text: string;
  onClick?: () => void;
  content?: boolean;
}

const SideListContents = (props: props) => {
  return (
    <>
      <li onClick={props.onClick} className={`${style["list-element"]} ${props.content?(style["active"]):(style["desactive"]) }`}>{props.text}</li>
    </>
  );
};

export { SideListContents };
