import React from "react";
import style from "./NavMobile.module.css";
import { LiItem } from "./LiItem";
import images from "../images/images";

interface INavMobile {
  open: boolean;
  setShowMenu: React.Dispatch<boolean>;
}
const NavMobile = ({ open, setShowMenu }: INavMobile) => {
  return (
    <div
      className={`${style["background-menu-mobile"]} ${
        !open && style["navmobile"]
      }`}
    >
      <div className={style["close-menu-mobile"]}>
        <img
          onClick={() => {
            setShowMenu(false);
          }}
          src={images.closemenumobile}
          alt=""
        />
      </div>
      <ul className={style["links-mobile"]}>
        <LiItem text="CURSOS" link="#" />
        <LiItem text="SAIBA MAIS" link="#" />
      </ul>
    </div>
  );
};

export { NavMobile };
