import React, { useCallback, useState } from "react";
import style from "./Header.module.css";
import images from "../images/images";
import { Nav } from "./Nav";
import { NavMobile } from "./NavMobile";

const Header = () => {
  const [showMenu, setShowMenu] = useState(false);
  return (
    <>
      <NavMobile setShowMenu={setShowMenu} open={showMenu} />
      <div className={style["header"]}>
        <div
          onClick={() => {
            setShowMenu(!showMenu);
          }}
          className={style["menu-mobile-icon"]}
        >
          <img src={images.menumobileicon} alt="menu-mobile" />
        </div>
        <img
          className={style["logo"]}
          src={images.logo}
          alt="logo-m3-academy"
        />
        <div className={style["search-box-wrapper"]}>
          <input
            placeholder="Buscar..."
            type="text"
            className={style["search-box-input"]}
          />
          <img src={images.searchicon} alt="search" />
        </div>
        <div className={style["login-and-cart-wrapper"]}>
          <div className={style["login-wrapper"]}>
            <a href="">Entrar</a>
          </div>
          <div className={style["cart-wrapper"]}>
            <img src={images.carticon} alt="" />
          </div>
        </div>
      </div>
      <Nav />
    </>
  );
};

export { Header };
