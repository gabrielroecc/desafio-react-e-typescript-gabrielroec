import React from "react";

interface IProps {
  link: string;
  text: string;
}

const LiItem = (props: IProps) => {
  return (
    <li>
      <a href={props.link}>{props.text}</a>
    </li>
  );
};
export { LiItem };
