import { ReactNode, useEffect, useState } from "react";
import React from "react";
import style from "./FooterElements.module.css";


interface IProps {
    title:string
    children?: ReactNode
}

const FooterElements = (props: IProps) =>{
    const [footer, setfooter] = useState(false)

    

    function useWindowSize()

    {const [windowSize, setWindowSize] = useState({
        width: 0,
        height:0,
    })
    useEffect(() => {
        function handleResize(){
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            })
        }

        window.addEventListener("resize", handleResize);
        handleResize();

        return () => window.removeEventListener("resize", handleResize);
    },[])

    return windowSize;
}

    const size = useWindowSize();

    const click = () =>{
        setfooter(!footer)
    }

    return <div className={style["footer-contents"]}>
        <h3 className={style["footer-titles"]}>{props.title}</h3><span className={style["expand-list-icon"]} onClick={click}>+</span>
        {footer || size.width > 540?<><ul className={style["footer-links-list"]}>
            {props.children}
        </ul></>:<></>}
        
    </div>
}

export {FooterElements}