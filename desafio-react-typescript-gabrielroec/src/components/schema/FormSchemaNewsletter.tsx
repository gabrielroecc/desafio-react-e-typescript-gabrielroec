import * as Yup from "yup";
export default Yup.object().shape({
  email: Yup.string()
    .email("Seu E-mail precisa um arroba")
    .required("Verifique se nos informou o e-mail correto"),
});
