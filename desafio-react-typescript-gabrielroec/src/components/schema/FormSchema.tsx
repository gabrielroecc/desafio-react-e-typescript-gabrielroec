import * as Yup from "yup";
import { cpf as validate } from "cpf-cnpj-validator";
import parse from "date-fns/parse";
import "yup-phone";

const today = new Date();

export default Yup.object().shape({
  name: Yup.string()
    .min(3, "Digite mais que duas letras")
    .required("Campo obrigatório"),
  email: Yup.string()
    .email("Seu E-mail precisa um arroba")
    .required("Campo obrigatório"),
  cpf: Yup.string()
    .min(11, "CPF deve ter no mínimo 11 dígitos")
    .test("cpf-invalid", "CPF Inválido", (cpf = "") => validate.isValid(cpf))
    .required("Campo obrigatório"),
  datadenascimento: Yup.date()
    .transform(function (value, originalValue) {
      if (this.isType(value)) {
        return value;
      }
      const result = parse(originalValue, "dd.MM.yyyy", today);
      return result;
    })
    .typeError("Verifique se os dados estão escritos corretamente")
    .required("Campo obrigatório")
    .min("1950-12-31", "O perído de ano informado é muito antigo")
    .max(
      today,
      "O data informado é maior que a data atual. Verifique se a data está correta."
    ),
  tel: Yup.string()
    .phone("BR", true, "Número inválido")
    .required("Campo obrigatório"),
  termos: Yup.bool().oneOf([true], "Campo obrigarótio"),
  insta: Yup.string()
    .min(3, "Verifique se informou o instagram corretamente")
    .required("Campo obrigatódio"),
});
