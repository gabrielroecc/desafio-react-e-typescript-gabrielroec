import React from "react";
import style from "./Newsletter.module.css";
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FormikHelpers,
  useField,
} from "formik";
import FormSchemaNewsletter from "./schema/FormSchemaNewsletter";

interface IFormikValues {
  email: string;
}

const initialValues = {
  email: "",
};

const Newsletter = () => {
  const handleFormikSubmit = (values: IFormikValues, actions: any) => {
    console.log(values);
    actions.resetForm();
  };

  return (
    <Formik
      validationSchema={FormSchemaNewsletter}
      onSubmit={handleFormikSubmit}
      initialValues={initialValues}
    >
      {({ errors, touched }) => (
        <div className={style["newsletter-wrapper"]}>
          <div className={style["newsletter-content"]}>
            <Form>
              <div className="input-wrapper">
                <label
                  className={style["label-newsletter"]}
                  htmlFor="newsletter"
                >
                  ASSINE NOSSA NEWSLETTER
                </label>
                <Field
                  className={`${style["newsletter-input"]} ${
                    errors.email && touched.email && style["invalid"]
                  }`}
                  name="email"
                  id="email"
                />

                <button className={style["newsletter-button"]} type="submit">
                  ENVIAR
                </button>
              </div>
              <div className={style["form-feedback-wrapper"]}>
                <ErrorMessage
                  name="email"
                  className={style["form-feedback"]}
                  component="span"
                />
              </div>
            </Form>
          </div>
        </div>
      )}
    </Formik>
  );
};

export { Newsletter };
